let vue = new Vue(
{
    el: "#vue-app",
    data: {
        projects: [],
        categories: [],
        filterCat: "Alle",
        newSchool:"HTL Ottakring",
        newProjectname:"VueJs",
        newCategory:"HTL"
    },
    methods: {
        addNewproject() {
            console.log("addNewproject");
            axios.post('/projects', 
            {
                school: this.newSchool,
                title: this.newProjectname,
                category:this.newCategory
            }).then(response => {
                console.log(response);
                this.projects = response.projects;
            });

        },
        loadData() {
            axios.get('/projects')
            .then(result => this.projects = result.data)
            .catch(e => {console.log(e.error)});

            axios.get('/categories')
            .then(result => this.categories = result.data)
            .catch(e => {console.log(e.error)});
           
        },
        filter(cat) {
            this.filterCat = cat;
        },
        delProj(id) {
            axios.delete('/project/'+id).
            then( e => this.projects = e.data).
            catch(e => console.log(e.error));
        }
    },
    
    computed: {
        filteredProjects() {
            if (this.filterCat == "Alle"){
                return this.projects;
            }
            return this.projects.filter(proj => proj.category == this.filterCat);
        }
    },
    mounted () {
        this.loadData();
    }
}

);