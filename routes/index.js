var express = require('express');
var router = express.Router();
var data = require('../model/jugendforscht2017');

/* GET home page. */
router.get('/projects', function(req, res, next) {
  res.status(200).send(data.getProjects());
});

router.post('/projects', function(req, res, next) {
  console.log(`post ${req.body}`);
  console.log(req.body);

  let result = data.addNewProject(req.body);

  res.status(200).send(result);
  
});

router.get('/categories', function(req, res, next) {
  res.status(200).send(data.getCategories());
});

router.delete('/project/:id', function(req, res, next) {
  res.status(200).send(data.deleteProject(req.params.id));
});


module.exports = router;
