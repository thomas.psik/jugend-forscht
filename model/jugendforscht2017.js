var projects = 
 [
        {"id":20171,
		"category": "SonderVolksschule",
        "school": "VS  Reichenau",
        "title": "Auf dem Weg durch die Forscherstraße - Forscherexperten auf dem Weg"},
        {"id":20172,
        "category": "SonderVolksschule",
        "school": "VS Brixlegg",
        "title": "Lumineszenz"},
        {"id":20173,
        "category": "SonderVolksschule",
        "school": "VS Stanz",
        "title": "Die magnetischen 16"},
        {"id":20174,
		"category": "Schulklasse",
        "school": "NMS Zirl",
        "title": "Spaghetti - außer lecker noch viel mehr!"},
        {"id":20175,
		"category": "Schulklasse",
        "school": "NMS Absam",
        "title": "Kristalle - wir lassen es glitzern!"},
        {"id":20176,
		"category": "Schulklasse",
        "school": "NMS Gabelsberger",
        "title": "Ein Polymer hat´s gar nicht schwer, vom Wasserglas ins Schwarze Meer"},
        {"id":20177,
		"category": "Schulklasse",
        "school": "NMS Zirl",
        "title": "Seife selbstgemacht"},
        {"id":20178,
		"category": "Schulklasse",
        "school": "PTS Brixlegg",
        "title": "Kompakt verpackt"},
        {"id":20179,
		"category": "Schulklasse",
        "school": "PTS Schwaz",
        "title": "Schulzentrum Schwaz goes „Elektro“"},
        {"id":201710,
		"category": "Kleingruppe",
        "school": "BG BRG Kufstein",
        "title": "Firlefranz"}, 
        {"id":201711,
        "category": "Kleingruppe",
        "school": "MS Clemens Holzmeister Landeck",
        "title": "Untersuchung unterschiedlicher regenerativer Energiequellen + zur Optimierung des Energiehaushaltes von Gemeinden und Städten am Beispiel der Stadt Landeck"},
        {"id":201712,
		"category": "Kleingruppe",
        "school": "BG BRG Kufstein",
        "title": "Getränkespendeautomat"},
        {"id":201713,
		"category": "Kleingruppe",
        "school": "NMS Zirl",
        "title": "Mr. Robbie Music"}
];

var categories = ["Alle", "SonderVolksschule", "Schulklasse", "Kleingruppe"];

function getProjects() {
    return projects;
}

function getCategories() {
    return categories;
}

function deleteProject(id) {
    return projects = projects.filter(pro => pro.id != id);
}

// newProject.school, ist ein neues Project aber ohne id!
function addNewProject(newProjectIn) {
    let maxId = -1;
    for (let proj of projects) {
        if (proj.id > maxId) {
            maxId = proj.id;
        }
    }
    maxId += 1;
    
    newProjectIn.id = maxId;

    projects.push(newProjectIn);

    return projects;
}

module.exports = {
    getProjects,
    getCategories,
    deleteProject,
    addNewProject
}